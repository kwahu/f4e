﻿using UnityEngine;
using System.Collections;
using RAIN.BehaviorTrees;
using RAIN.Minds;

//Arkadiusz Kwasny - 04.08.2014

public class AIController : MonoBehaviour
{
		private RAIN.Core.AIRig rig;
		private string currentTreeName = null;


		
		/*
		private bool _lookForPlayer = false;
		public bool LookForPlayer {
				get {
						return _lookForPlayer;
				}
				set {
						_lookForPlayer = value;
						if (rig == null)
								Debug.LogError ("AIRig component is missing in : " + name);
						rig.AI.WorkingMemory.SetItem<bool> ("lookForPlayer", _lookForPlayer);
				}
		}*/

		private void Awake ()
		{
				rig = GetComponent<RAIN.Core.AIRig> ();
				if (rig == null)
						Debug.LogError ("AIRig component is missing in : " + name);
		}

		// Use this for initialization
		public virtual void Start ()
		{


		}

		public bool RunTree (string name)
		{
				if (rig != null && currentTreeName != name) {
						BTAsset behavior = Resources.Load ("AI/BehaviorTrees/" + name) as BTAsset;
		
						if (behavior == null)
			{
				Debug.LogError("Failed to load " + "AI/BehaviorTrees/" + name);
								return false;
			}
						currentTreeName = name;
			(rig.AI.Mind as BasicMind).SetBehavior( behavior, new System.Collections.Generic.List<BTAssetBinding>() );
						rig.AI.AIInit ();
						return true;
				} else
						return false;
		}

		public bool GetBool (string name)
		{
				return rig.AI.WorkingMemory.GetItem<bool> (name);
		}

		public void SetBool (string name, bool value)
		{
				rig.AI.WorkingMemory.SetItem<bool> (name, value);
		}

		public int GetInt (string name)
		{
				return rig.AI.WorkingMemory.GetItem<int> (name);
		}

		public void SetInt (string name, int value)
		{
				rig.AI.WorkingMemory.SetItem<int> (name, value);
		}

		public GameObject GetGameObject (string name)
		{
				return rig.AI.WorkingMemory.GetItem<GameObject> (name);
		}

		public void SetGameObject (string name, GameObject value)
		{
				rig.AI.WorkingMemory.SetItem<GameObject> (name, value);
		}

		public Vector3 GetVector (string name)
		{
				return rig.AI.WorkingMemory.GetItem<Vector3> (name);
		}
	
		public void SetVector (string name, Vector3 value)
		{
				rig.AI.WorkingMemory.SetItem<Vector3> (name, value);
		}

		public Vector3 GetTarget (string name)
		{
				//RAIN.Motion.MoveLookTarget
				//Debug.Log (rig.AI.Motor.moveTarget.VectorTarget);
				//Debug.Log(rig.AI.WorkingMemory.GetItem<RAIN.Motion.MoveLookTarget>("next"));
				return rig.AI.Motor.MoveTarget.Position;
				/*MoveLookTargetType tTargetType = moveTarget.TargetType;
		if (tTargetType == MoveLookTargetType.Variable)
		{
			MoveLookTarget tTarget = MoveLookTarget.GetTargetFromVariable(moveTarget.VariableMemory, moveTarget.VariableTarget, moveTarget.CloseEnoughDistance, null);
			MoveLookTarget.MoveLookTargetType tTargetType = tTarget.TargetType;
		}*/
	
		}
	
}