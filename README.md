# README #

Please find the steps in the [WIKI](https://bitbucket.org/kwahu/f4e/wiki/Home)

# What is this repository for? #

Quick summary
This is a template to help you get started with VR simulations.

It consists of the following elements
1. mobile device sensor support
1. stereo split screen rendering
1. gamepad control
1. AI (RAIN)
1. support for animated 3d characters
1. dialogue system (DIALOGUER)


What is still missing
1. network communication - multiplayer
1. a 3d face animations
1. IK interaction with objects
1. speech synthesis
1. camera hand gesture recognition
1. ???


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact